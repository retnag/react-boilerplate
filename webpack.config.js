/* eslint-disable */

const path = require("path");

module.exports = {
    entry: "./src/index.tsx",
    output: {
        filename: "bundle.js",
        path: __dirname + "/build",
        publicPath: "/build/",
    },
    devServer: {
        historyApiFallback: true,
    },

    // Enable sourcemaps for debugging webpack's output.
    devtool: "source-map",

    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        // root: __dirname,
        extensions: [".ts", ".tsx", ".js", ".json"],
        modules: [path.resolve("./src"), path.resolve("./node_modules")],
    },

    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: "awesome-typescript-loader",
                exclude: /node_modules/,
            },
        ],
    },

    plugins: [],

    // When importing a module whose path matches one of the following, just
    // assume a corresponding global variable exists and use that instead.
    // This is important because it allows us to avoid bundling all of our
    // dependencies, which allows browsers to cache those libraries between builds.
    externals: {
        react: "React",
        "react-dom": "ReactDOM",
    },
    node: {
        fs: "empty",
        child_process: "empty",
    },
};
