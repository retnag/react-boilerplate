/* eslint-disable */
import * as React from "react";

import {
    AppBar,
    Button,
    ThemeProvider,
    Toolbar,
    Typography,
} from "@material-ui/core";

import { defaultTheme } from "theme";
import { routes } from "routes/routeMappings";
import styled from "styled-components";
import { useHistory } from "react-router-dom";

interface Props {
    children: React.ReactNode | React.ReactNode[];
}

export const Header = (props: Props): JSX.Element => {
    const history = useHistory();

    return (
        <ThemeProvider theme={defaultTheme}>
            <Container>
                <HeaderArea>
                    <AppBar>
                        <Toolbar>
                            <Typography
                                variant="h6"
                                style={{ marginRight: 20 }}
                            >
                                React boilerplate
                            </Typography>
                            {routes.map((route) => (
                                <AppbarBtn key={route.path}>
                                    <Button
                                        variant="contained"
                                        color="secondary"
                                        onClick={() => {
                                            history.push(route.path);
                                        }}
                                    >
                                        {route.name}
                                    </Button>
                                </AppbarBtn>
                            ))}
                        </Toolbar>
                    </AppBar>
                </HeaderArea>
                <ContentArea>{props.children}</ContentArea>
            </Container>
        </ThemeProvider>
    );
};

const HeaderArea = styled.div`
     {
        color: #fff;
    }
`;
const ContentArea = styled.div`
     {
        padding: 20px;
    }
`;

const Container = styled.div`
     {
        width: 100%;
        height: 100%;
        padding-top: 70px;
    }
`;
const AppbarBtn = styled.div`
     {
        margin: 5px;
    }
`;
