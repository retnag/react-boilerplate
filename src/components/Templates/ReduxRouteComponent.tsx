/* eslint-disable */
import * as React from "react";

import { ConnectedProps, connect } from "react-redux";

import { Dispatch } from "redux";
import { ReduxStore } from "redux/store";
import { RouteComponentProps } from "react-router";

interface MatchParams {}

interface OwnProps extends RouteComponentProps<MatchParams> {}
interface State {}

const mapState = (store: ReduxStore) => {
    return {};
};

const mapDispatch = (dispatch: Dispatch) => {
    return {};
};

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;
type Props = PropsFromRedux & OwnProps;

export class Template extends React.PureComponent<Props, State> {
    static defaultProps: Partial<OwnProps> = {};
}

export default connector(Template);
