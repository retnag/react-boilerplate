/* eslint-disable no-magic-numbers */
import { indigo, yellow } from "@material-ui/core/colors";

import { createMuiTheme } from "@material-ui/core/styles";

export const defaultTheme = createMuiTheme({
    palette: {
        primary: {
            main: indigo[900],
        },
        secondary: {
            main: yellow[600],
        },
    },
});
