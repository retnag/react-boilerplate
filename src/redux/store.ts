import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import thunk from "redux-thunk";

const rootReducer = combineReducers({});

// nescessary for redux devtools to work:
const composeEnhancers =
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (<any>window).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(thunk))
);

export type ReduxStore = ReturnType<typeof store.getState>;
