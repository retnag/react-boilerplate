import { Home } from "routes/Home";
import React from "react";

export interface RouteMapping {
    path: string;
    component: JSX.Element;
    name: string;
}

export const routes: RouteMapping[] = [
    {
        path: "/home",
        component: <Home></Home>,
        name: "Home",
    },
];
