/**
 * @author Manz Günter
 */
import * as React from "react";

export class Home extends React.PureComponent {
    render(): JSX.Element {
        return <div>Hello World</div>;
    }
}
