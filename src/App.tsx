/**
 * @author Manz Günter
 */
import * as React from "react";

import { BrowserRouter, Route, Switch } from "react-router-dom";

import { Header } from "components/Header";
import { routes } from "routes/routeMappings";

export class App extends React.Component {
    render(): JSX.Element {
        return (
            <BrowserRouter basename="/">
                <Header>
                    <Switch>
                        {routes.map((rt) => (
                            <Route key={rt.path} exact path={rt.path}>
                                {rt.component}
                            </Route>
                        ))}
                    </Switch>
                </Header>
            </BrowserRouter>
        );
    }
}
