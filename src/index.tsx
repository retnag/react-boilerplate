import * as React from "react";
import * as ReactDOM from "react-dom";

import { App } from "App";
import { MuiThemeProvider } from "@material-ui/core";
import { Provider } from "react-redux";
import { defaultTheme } from "theme";
import { store } from "redux/store";

ReactDOM.render(
    <MuiThemeProvider theme={defaultTheme}>
        <Provider store={store}>
            <App />
        </Provider>
    </MuiThemeProvider>,
    document.getElementById("app_root")
);
