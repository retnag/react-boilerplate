#!/bin/bash
#
# Automatically add issue number to every commit message.
#

if [ -z "$BRANCHES_TO_SKIP" ]; then
  BRANCHES_TO_SKIP=(master develop)
fi

BRANCH_NAME="$(git symbolic-ref --short HEAD)"
if [[ "$?" != "0" ]]; then 
  exit 0;
fi
BRANCH_EXCLUDED=$(printf "%s\n" "${BRANCHES_TO_SKIP[@]}" | grep -c "^$BRANCH_NAME$")

# skip on release branch
[[ "$BRANCH_NAME" =~ ^release(-[0-9]+(.[0-9]+)*)?* ]] && exit 0

regex="^([0-9]+)-.*"
if [[ "$BRANCH_NAME" =~ $regex ]]; then
    ISSUE_NUMBER="${BASH_REMATCH[1]}"
fi
if [[ "$ISSUE_NUMBER" == "" ]]; then
  echo "no issue number found in branch name, skipping hook"
  exit 0
fi

COMMIT_MESSAGE=$(<$1)

if [[ "$BRANCH_NAME" != "" ]] && (( "$BRANCH_EXCLUDED" == 0 )); then
  regex="\[#$ISSUE_NUMBER\] .*"
  if [[ ! "$COMMIT_MESSAGE" =~ $regex ]]; then
    echo "[#$ISSUE_NUMBER] $COMMIT_MESSAGE" > $1
  fi
fi
