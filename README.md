# react-boilerplate

This is a boilerplate project configured with my usual workflow with react in mind.

# Use case

-   Frontend (web) applications using typescript and react
-   You want automated code-quality enforcements (prettier, eslint and typescript compiler options)

Installed packages include:

-   webpack
-   typescript
-   prettier
-   eslint
-   husky - configured woth some hooks
-   react
-   redux
-   styled-components
-   color
-   lodash
-   moment
-   axios
-   flatted

# Code quality tools

-   **prettier** is installed and configured to ensure consistent coding style
-   **eslint** enforces best practices
-   additionally the **typescript** compiler has a few extra rules set, like "noimplicitany".
-   **git hooks**
    -   lint-staged precommit hook which checks staged files using prettier and elsint. If any issues are found with the tools, the commit will fail. This ensures consistent code style, and higher quality code.
    -   for every commit the issue number for that branch is prepended automatically (exception branches can be configured)
